using MathNet.Numerics.Distributions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupervisedLearning.EpsilonGreedy
{
    class Program
    {
        private static int n_actions = 4;

        private static double CalcoloQFunction(double Qk, int nk, double rk)
        {
            return (Qk + (1 / (float)nk) * (rk - Qk));
        }

        static void Main(string[] args)
        {
            List<String> actions;
            List<int> frequencyAction;
            List<double> qFunction;

            InizializationAgent(out actions, out frequencyAction, out qFunction);

            //enviroment
            List<double> reward = InizializationRewards();
            ContinuousUniform UniformDist = new ContinuousUniform(0,1);
            DiscreteUniform UniformDistDiscrete = new DiscreteUniform(0,n_actions-1);

            Console.WriteLine("Give the Epsilon min between [0,1] for the tradeoff between exploration and exploitation)");
            Console.WriteLine("you must use the , to write the number");
            double epsilon_min = AcquireEpsilonMin();
            int x = 0;
            while (x < 100)
            {
                double epsilon = UniformDist.Sample();
                var q_actual = qFunction.Max();
                var index = qFunction.IndexOf(q_actual);
                var a_actual = actions[index];
                if (epsilon < epsilon_min)
                {
                    Exploration(actions, UniformDistDiscrete, ref index, ref a_actual);
                }
                q_actual = ImplentationActionAndCalcolateQFunction(frequencyAction, qFunction, reward, q_actual, index);
                x++;
            }
            Console.ReadLine();
        }

        private static double AcquireEpsilonMin()
        {
            double epsilon_min = -1;
            bool input_range = false;
            bool correct_input = false;

            while (!correct_input && !input_range)
            {
                String stream = Console.ReadLine();
                if (!double.TryParse(stream, out epsilon_min))
                {
                    correct_input = true;
                }
                if (epsilon_min >= 0 && epsilon_min <= 1)
                {
                    input_range = true;
                }
            }
            return epsilon_min;
        }

        private static double ImplentationActionAndCalcolateQFunction(List<int> frequencyAction, List<double> qFunction, List<double> reward, double q_actual, int index)
        {
            q_actual = qFunction[index];
            var rewardForAction = reward[index];
            int nk = frequencyAction[index]++;
            qFunction[index] = CalcoloQFunction(q_actual, nk, rewardForAction);
            return q_actual;
        }

        private static void Exploration(List<String> actions, DiscreteUniform UniformDistDiscrete, ref int index, ref string a_actual)
        {
            var trovato = false;
            var a_ref = a_actual;
            while (trovato == false)
            {
                index = UniformDistDiscrete.Sample();
                a_actual = actions[index];
                if (!a_actual.Equals(a_ref))
                {
                    trovato = true;
                    Console.WriteLine("Action : {0} ", a_actual);
                }
            }

        }

        private static List<double> InizializationRewards()
        {
            List<double> reward = new List<double>();
            double mean = 0;
            double stdDev = 1;

            Normal normalDist = new Normal(mean, stdDev);
            for (int i = 0; i < n_actions; i++)
            {
                reward.Add(normalDist.Sample());
            }
            return reward;
        }

        private static void InizializationAgent(out List<String> actions, out List<int> frequencyAction, out List<double> qFunction)
        {

            //agent
            actions = new List<String>();
            actions.Add("Up");
            actions.Add("Down");
            actions.Add("Left");
            actions.Add("Right");

            frequencyAction = new List<int>();
            frequencyAction.Add(10);
            frequencyAction.Add(20);
            frequencyAction.Add(15);
            frequencyAction.Add(12);

            qFunction = new List<double>();
            qFunction.Add(0.3);
            qFunction.Add(0.4);
            qFunction.Add(0.2);
            qFunction.Add(0.1);
        }
    }
}
